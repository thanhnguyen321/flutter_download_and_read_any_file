import 'dart:io';

import 'package:dio/dio.dart';
import 'package:external_path/external_path.dart';
import 'package:flutter/material.dart';
import 'package:open_file_plus/open_file_plus.dart';
import 'package:read_file/check_permission.dart';
import 'package:path/path.dart' as Path;

class FileList extends StatefulWidget {
  const FileList({super.key});

  @override
  State<FileList> createState() => _FileListState();
}

var dataList = [
  {
    "id": "2",
    "title": "file Video 1",
    "url":
        "https://vz-123412341234.b-cdn.net/1234-1234-1234-1234-123412341234/play_720p.mp4"
  },
  {
    "id": "6",
    "title": "file PDF 6",
    "url": "https://www.africau.edu/images/default/sample.pdf"
  },
  {
    "id": "10",
    "title": "file PDF 7",
    "url": "https://www.africau.edu/images/default/sample.pdf"
  },
  {
    "id": "11",
    "title": "youtobe",
    "url": "https://www.youtube.com/watch?v=gF3b0yFfmKI"
  },
];

class _FileListState extends State<FileList> {
  bool isPermission = false;
  var checkAllPermissions = CheckPermission();

  checkPermission() async {
    var permision = await checkAllPermissions.isStoragePermission();
    if (permision) {
      setState(() {
        isPermission = true;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    checkPermission();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Demo'),
        ),
        body: isPermission
            ? ListView.builder(
                itemCount: dataList.length,
                itemBuilder: (context, index) {
                  return TitleList(
                    items: dataList[index],
                  );
                },
              )
            : TextButton(
                onPressed: () {
                  checkPermission();
                },
                child: const Text("Click me")));
  }
}

class TitleList extends StatefulWidget {
  TitleList({super.key, required this.items});
  final items;
  @override
  State<TitleList> createState() => _TitleListState();
}

class _TitleListState extends State<TitleList> {
  bool downloading = false;
  bool fileExists = false;
  double progress = 0;
  String fileName = '';
  late String filePath;
  late CancelToken cancelToken;

  startDownload() async {
    cancelToken = CancelToken();
    var storePath = await getDownloadForderPath();
    filePath = '$storePath/$fileName';
    try {
      await Dio().download(
        widget.items['url'],
        filePath,
        onReceiveProgress: (count, total) {
          setState(() {
            progress = (count / total);
          });
        },
        cancelToken: cancelToken,
      );
      setState(() {
        downloading = false;
        fileExists = true;
      });
    } catch (e) {
      setState(() {
        downloading = false;
      });
    }
  }

  cancelDownload() async {
    cancelToken.cancel();
    setState(() {
      downloading = false;
    });
  }

  Future<String> getDownloadForderPath() async {
    return await ExternalPath.getExternalStoragePublicDirectory(
        ExternalPath.DIRECTORY_DOWNLOADS);
  }

  checkFileExit() async {
    var storePath = await getDownloadForderPath();
    filePath = '$storePath/$fileName';
    bool fileExistCheck = await File(filePath).exists();
    setState(() {
      fileExists = fileExistCheck;
    });
  }

  openFile() {
    OpenFile.open(filePath);
  }

  @override
  void initState() {
    setState(() {
      fileName = Path.basename(widget.items['url']);
    });
    super.initState();
    checkFileExit();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        leading: IconButton(
          icon: fileExists && downloading == false
              ? const Icon(Icons.window)
              : const Icon(Icons.close),
          onPressed: () {
            fileExists && downloading == false ? openFile() : cancelDownload();
          },
        ),
        title: Text(widget.items['title']),
        trailing: IconButton(
          icon: fileExists
              ? const Icon(Icons.save)
              : downloading
                  ? Stack(
                      alignment: Alignment.center,
                      children: [
                        CircularProgressIndicator(
                          value: progress,
                          strokeWidth: 3,
                          backgroundColor: Colors.green,
                          valueColor:
                              const AlwaysStoppedAnimation<Color>(Colors.red),
                        ),
                        Text((progress * 100).toStringAsFixed(2))
                      ],
                    )
                  : const Icon(Icons.download),
          onPressed: () {
            fileExists && downloading == false ? openFile() : startDownload();
          },
        ),
      ),
    );
  }
}
